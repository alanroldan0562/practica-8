package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class vehiculo implements Comparable<vehiculo>{
	
	private int numeroNehiculo;
	private String marca;
	private int modelo;
	private float maxCarga;
	private LocalDate fechaAdquisicion;
	private ArrayList<viajes> viajesRealizados;
	
	public boolean cancelaViaje (viajes V){
		boolean prueba=false;
		return prueba;
	}
	
	public vehiculo(int numeroNehiculo, String marca, int modelo, float maxCarga, LocalDate fechaAdquisicion) {
		super();
		this.numeroNehiculo = numeroNehiculo;
		this.marca = marca;
		this.modelo = modelo;
		this.maxCarga = maxCarga;
		this.fechaAdquisicion = fechaAdquisicion;
	}

	public float getMaxCarga() {
		return maxCarga;
	}

	public void setMaxCarga(float maxCarga) {
		this.maxCarga = maxCarga;
	}

	public int getNumeroNehiculo() {
		return numeroNehiculo;
	}

	public String getMarca() {
		return marca;
	}

	public int getModelo() {
		return modelo;
	}

	public LocalDate getFechaAdquisicion() {
		return fechaAdquisicion;
	}

	public ArrayList<viajes> getViajesRealizados() {
		return viajesRealizados;
	}

	@Override
	public String toString() {
		return "vehiculo [numeroNehiculo=" + numeroNehiculo + ", marca=" + marca + ", modelo=" + modelo + ", maxCarga="
				+ maxCarga + ", fechaAdquisicion=" + fechaAdquisicion + ", viajesRealizados=" + viajesRealizados + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaAdquisicion == null) ? 0 : fechaAdquisicion.hashCode());
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + Float.floatToIntBits(maxCarga);
		result = prime * result + modelo;
		result = prime * result + numeroNehiculo;
		result = prime * result + ((viajesRealizados == null) ? 0 : viajesRealizados.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		vehiculo other = (vehiculo) obj;
		if (fechaAdquisicion == null) {
			if (other.fechaAdquisicion != null)
				return false;
		} else if (!fechaAdquisicion.equals(other.fechaAdquisicion))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (Float.floatToIntBits(maxCarga) != Float.floatToIntBits(other.maxCarga))
			return false;
		if (modelo != other.modelo)
			return false;
		if (numeroNehiculo != other.numeroNehiculo)
			return false;
		if (viajesRealizados == null) {
			if (other.viajesRealizados != null)
				return false;
		} else if (!viajesRealizados.equals(other.viajesRealizados))
			return false;
		return true;
	}

	@Override
	public int compareTo(vehiculo v) {
		// TODO Auto-generated method stub
		if(!this.marca.equals(v.getMarca()))
			return this.marca.compareTo(v.getMarca());
		return 0;
	}

}
